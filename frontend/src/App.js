import {Route} from "react-router-dom";
import Search from "./containers/Link/Search";

const App = () => (
	<Route path="/" exact component={Search}/>
);

export default App;
