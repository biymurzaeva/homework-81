import React from 'react';
import LinkForm from "../../components/LinkForm/LinkForm";
import './Link.css';
import {useDispatch, useSelector} from "react-redux";
import {createLink} from "../../store/actions/searchAction";

const Search = () => {
	const dispatch = useDispatch();
	const shortUrl = useSelector(state => state.responseServer.resUrl);

	const search = async url => {
		await dispatch(createLink(url));
	};

	let link = (
		<>
		</>
	);

	if (shortUrl) {
		link = (
			<>
				<p>Your link now looks like this:</p>
				<a href={shortUrl.originalUrl}>http://localhost:8000/{shortUrl.shortUrl}</a>
			</>
		);
	}

	return (
		<div className="Container">
			<h2>Shorten your link!</h2>
			<LinkForm
				onSubmit={search}
			/>
			{link}
		</div>
	);
};

export default Search;