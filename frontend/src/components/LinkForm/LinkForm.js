import React, {useState} from 'react';
import './LinkForm.css';

const LinkForm = ({onSubmit}) => {
	const [state, setState] = useState({
		url: '',
	});

	const inputChange = e => {
		const {name, value} = e.target
		setState(prevState => ({
			...prevState,
			[name]: value
		}));
	};

	const onSubmitForm = e => {
		e.preventDefault();

		onSubmit({...state});
	};

	return (
		<form onSubmit={onSubmitForm}>
			<div className="FormRow">
				<input
					type="text"
					name="url"
					value={state.url}
					onChange={inputChange}
				/>
			</div>
			<button type="submit">Shorten!</button>
		</form>
	);
};

export default LinkForm;