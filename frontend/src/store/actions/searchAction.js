import axios from "axios";

export const CREATE_LINK_REQUEST = 'CREATE_LINK_REQUEST';
export const CREATE_LINK_SUCCESS = 'CREATE_LINK_SUCCESS';
export const CREATE_LINK_FAILURE = 'CREATE_LINK_FAILURE';

export const createLinkRequest = () => ({type: CREATE_LINK_REQUEST});
export const createLinkSuccess = response => ({type: CREATE_LINK_SUCCESS, payload: response});
export const createLinkFailure = e => ({type: CREATE_LINK_FAILURE, payload: e});

export const createLink = url => {
	return async dispatch => {
		try {
			dispatch(createLinkRequest());
			const response = await axios.post('http://localhost:8000/links', url);
			dispatch(createLinkSuccess(response.data));
		} catch (e) {
			dispatch(createLinkFailure(e));
			throw e;
		}
	};
};