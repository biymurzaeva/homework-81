import {CREATE_LINK_FAILURE, CREATE_LINK_REQUEST, CREATE_LINK_SUCCESS} from "../actions/searchAction";

const initialState = {
	loading: false,
	error: false,
	resUrl: null,
};

const searchReducer = (state = initialState, action) => {
	switch (action.type) {
		case CREATE_LINK_REQUEST:
			return {...state, loading: true};
		case CREATE_LINK_SUCCESS:
			return {...state, loading: false, resUrl: action.payload};
		case CREATE_LINK_FAILURE:
			return {...state, error: true};
		default:
			return state;
	}
};

export default searchReducer;