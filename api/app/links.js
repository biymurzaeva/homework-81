const express = require('express');
const Link = require('../modules/Link');
const {nanoid} = require("nanoid");

const router = express.Router();

router.get('/links', async (req, res) => {
	try {
		const links = await Link.find();
		res.send(links);
	} catch (e) {
		res.sendStatus(500);
	}
});

router.get('/:shortUrl', async (req, res) => {
	try {
		const [link] = await Link.find({shortUrl: req.params.shortUrl});

		if (link) {
			res.status(301).redirect(link.originalUrl);
		} else {
			res.sendStatus(404).send({error: 'URL not found'});
		}

	} catch (e) {
		res.sendStatus(500);
	}
});

router.post('/links', async (req, res) => {
	if (!req.body.url) {
		return res.status(400).send({error: 'Data not valid'});
	}

	const linkData = {
		originalUrl: req.body.url,
		shortUrl: nanoid(7)
	};

	const link = new Link(linkData);

	try {
		await link.save();
		res.send(link);
	} catch {
		res.sendStatus(400).send({error: 'Data not valid'});
	}
});

module.exports = router;