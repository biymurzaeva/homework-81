const express = require('express');
const cors = require('cors');
const links = require('./app/links');
const mongoose = require('mongoose');

const app = express();
app.use(express.json());
app.use(cors());

const port = 8000;

app.use('', links);

const run = async () => {
	await mongoose.connect('mongodb://localhost/search');

	app.listen(port, () => {
		console.log(`Server started on ${port} port`);
	});
};

run().catch(e => console.log(e))