const mongoose = require('mongoose');

const LinkSchema = new mongoose.Schema({
	originalUrl: {
		type: String,
		required: true
	},
	shortUrl: String,
},
	{
		versionKey: false
	}
);

const Link = mongoose.model('Link', LinkSchema);
module.exports = Link;